create table Local_Guestbook_Guestbook (
	uuid_ VARCHAR(75) null,
	userId LONG not null primary key,
	Name VARCHAR(75) null,
	Message VARCHAR(75) null,
	Date VARCHAR(75) null,
	createDate DATE null,
	ImageUrl VARCHAR(200) null
);