/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.omb.guestbookdb.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.omb.guestbookdb.model.Guestbook;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Guestbook in entity cache.
 *
 * @author TT
 * @generated
 */
public class GuestbookCacheModel
	implements CacheModel<Guestbook>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof GuestbookCacheModel)) {
			return false;
		}

		GuestbookCacheModel guestbookCacheModel = (GuestbookCacheModel)obj;

		if (userId == guestbookCacheModel.userId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, userId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", Name=");
		sb.append(Name);
		sb.append(", Message=");
		sb.append(Message);
		sb.append(", Date=");
		sb.append(Date);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", ImageUrl=");
		sb.append(ImageUrl);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Guestbook toEntityModel() {
		GuestbookImpl guestbookImpl = new GuestbookImpl();

		if (uuid == null) {
			guestbookImpl.setUuid("");
		}
		else {
			guestbookImpl.setUuid(uuid);
		}

		guestbookImpl.setUserId(userId);

		if (Name == null) {
			guestbookImpl.setName("");
		}
		else {
			guestbookImpl.setName(Name);
		}

		if (Message == null) {
			guestbookImpl.setMessage("");
		}
		else {
			guestbookImpl.setMessage(Message);
		}

		if (Date == null) {
			guestbookImpl.setDate("");
		}
		else {
			guestbookImpl.setDate(Date);
		}

		if (createDate == Long.MIN_VALUE) {
			guestbookImpl.setCreateDate(null);
		}
		else {
			guestbookImpl.setCreateDate(new Date(createDate));
		}

		if (ImageUrl == null) {
			guestbookImpl.setImageUrl("");
		}
		else {
			guestbookImpl.setImageUrl(ImageUrl);
		}

		guestbookImpl.resetOriginalValues();

		return guestbookImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		userId = objectInput.readLong();
		Name = objectInput.readUTF();
		Message = objectInput.readUTF();
		Date = objectInput.readUTF();
		createDate = objectInput.readLong();
		ImageUrl = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(userId);

		if (Name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(Name);
		}

		if (Message == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(Message);
		}

		if (Date == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(Date);
		}

		objectOutput.writeLong(createDate);

		if (ImageUrl == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(ImageUrl);
		}
	}

	public String uuid;
	public long userId;
	public String Name;
	public String Message;
	public String Date;
	public long createDate;
	public String ImageUrl;

}